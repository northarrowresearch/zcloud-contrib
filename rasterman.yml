name: Raster Manager
shortname: rasterman
version: 6.1.8

description: "Raster Manager (rasterman for short) is a open-source manipulator for raster files like GeoTiff, IMG etc. prominent features include: csv2raster, mosaic"
author: Philip Bailey, Matt Reimer
command: rasterman.exe

dates:
  added: "Aug 10, 2015"
  released: "Jan 15, 2015"
  updated: "Jun 8, 2015"

categories:
  - Raster
  - Geomorphic
  - Open Source

urls: 
  download: http://releases.northarrowresearch.com/RasterManager/Win64/RasterManager_6_1_8_x64.zip
  docs: http://www.northarrowresearch.com/2015/07/08/rasterman-version-6.1.8-released.html
  license: https://raw.githubusercontent.com/NorthArrowResearch/rasterman/master/LICENSE.txt

instructions:
  - "Follow the wizard to populate your numerous parameters"
  - "When the download buttons appear, click them both"
  - "make sure your rasterman.bat file is next to your rasterman.exe file"
  - "double click the rasterman.bat file"

features:
- name: raster
  description: Display basic properties (rows, cols etc) for a raster.
- name: stats
  description: "Display specific statistics (mean, max, min, etc) for a raster."
- name: compare
  description: "Compare two rasters: check divisible, orthogonal, concurrent and by cell"
- name: bilinear
  description: Bilinear resample of a raster theo produce a new raster.
- name: copy
  description: Copy a raster to produce a new raster with the specified extent.
- name: mosaic
  description: Stitch two or more overlappint rasters.
- name: combine
  description: Combine multiple rasters using one or more methods.
- name: makeconcurrent
  description: Make all input rasters concurrent.
- name: mask
  description: Mask one raster using another raster or a vector.
- name: maskval
  description: Mask one raster using one of its values.
- name: setnull
  description: Remove values a number of different ways (high-pass, low-pass etc.)
- name: math
  description: Perform basic math on two rasters or a raster and a number.
- name: invert
  description: Create a raster from nodata values of another.
- name: filter
  description: Perform operations like \"mean\" and \"range\" over a moving window.
- name: normalize
  description: Normalize a raster.
- name: uniform
  description: Make a uniform raster.
- name: fill
  description: Optimized Pit Removal.
- name: dist
  description: Euclidean distance calculation.
- name: linthesh
  description: Linear thresholding of a raster.
- name: areathresh
  description: Thresholding of features below a certain area.
- name: smoothedges
  description: Smooth the edges of features delineated by NoData.
- name: hillshade
  description: Create a hillshade raster.
- name: slope
  description: Create a slope raster.
- name: png
  description: Create a PNG image copy of a raster.
- name: csv2raster
  description: Create a raster from a .csv file
- name: raster2csv
  description: Create a raster from a .csv file
- name: extractpoints
  description: Extract point values from a raster using a csv.

params:
  - name: Operation
    type: select
    tip: Choose an operation
    options: 
      - name: raster
        description: Display basic properties (rows, cols etc) for a raster.
        params: 
          - name: "Input raster"
            type: text
            validation: filepath
            placeholder: C:\some\dir\input.tif
            tip: Full path to the input raster file
      - name: stats
        description: Display specific statistics (mean, max, min, etc) for a raster.
        params: 
          - name: "Input raster"
            type: text
            validation: filepath
            placeholder: C:\some\dir\input.tif
            tip: Full path to the input raster file
      - name: compare
        description: "Compare two rasters: check divisible, orthogonal, concurrent and by cell"
      - name: bilinear
        description: Bilinear resample of a raster theo produce a new raster.
      - name: copy
        description: Copy a raster to produce a new raster with the specified extent.
      - name: mosaic
        description: Stitch two or more overlappint rasters.
      - name: combine
        description: Combine multiple rasters using one or more methods.
      - name: makeconcurrent
        description: Make all input rasters concurrent.
      - name: mask
        description: Mask one raster using another raster or a vector.
      - name: maskval
        description: Mask one raster using one of its values.
      - name: setnull
        description: Remove values a number of different ways (high-pass, low-pass etc.)
      - name: math
        description: Perform basic math on two rasters or a raster and a number.
      - name: invert
        description: Create a raster from nodata values of another.
      - name: filter
        description: Perform operations like \"mean\" and \"range\" over a moving window.
      - name: normalize
        description: Normalize a raster.
      - name: uniform
        description: Make a uniform raster.
      - name: fill
        description: Optimized Pit Removal.
      - name: dist
        description: Euclidean distance calculation.
      - name: linthesh
        description: Linear thresholding of a raster.
      - name: areathresh
        description: Thresholding of features below a certain area.
      - name: smoothedges
        description: Smooth the edges of features delineated by NoData.
      - name: hillshade
        description: Create a hillshade raster.
      - name: slope
        description: Create a slope raster.
      - name: png
        description: Create a PNG image copy of a raster.
      - name: csv2raster
        description: Create a raster from a .csv file
      - name: raster2csv
        description: Create a raster from a .csv file
      - name: extractpoints
        description: Extract point values from a raster using a csv.


      - name: "fill"
        params: 
          - name: "Input raster"
            type: text
            validation: filepath
            placeholder: C:\some\dir\input.tif
            tip: Full path to the input raster file
          - name: "Output raster"
            type: text
            validation: filepath
            placeholder: C:\some\dir\output.tif
            tip: Full path to the output raster file            
      - name: "dist"
        description: Euclidean distance calculation
      - name: "mosaic"
        description: Mosaic of rasters
        params: 
          - name: "Input Rasters"
            type: multitext
            validation: filepath
            placeholder: c:\some\dir\input.tif
            tip: one or more file paths. 
            delim: ";"
          - name: "Output raster"
            type: text
            validation: filepath
            placeholder: C:\some\dir\output.tif
            tip: Full path to the output raster file      
      - name: "math"
        description: Raster Math operations
        params:
          - name: "Math Operation"
            type: select
            tip: Choose a MATH operation
            options:
              - name: "subtract"
              - name: "add"
              - name: "multiply"
          - name: "Input raster"
            type: text
            validation: filepath
            placeholder: C:\some\dir\input.tif
            tip: Full path to the input file
          - name: "Math Operator"
            type: or
            options: 
             - params: 
                - name: "File Operator"
                  type: text
                  validation: filepath
                  tip: Full path to the second file
                  placeholder: C:\some\dir\input.tif
             - params: 
                - name: "Numerical Operator"
                  type: number
                  validation: number
                  placeholder: 12345.234
                  tip: numerical operator
          - name: "Output Raster"
            type: text
            validation: filepath
            tip: Full path to the output file

