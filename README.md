# Z-Cloud Tool Contributor Data files

This repo contains all the tool definitions for the ZCloud portal. When getting a tool ready to push to ZCloud you can use these files as an example and contribute your own.

## How to make a contribution:

1. Fork this repo to your own [Bitbucket account](https://bitbucket.org).
2. Clone this repo to your local box *(or not. see note #2)*.
3. Test your "YML" file online using the [YML Tester](http://zcloud.jekyll.com.s3-website-us-west-2.amazonaws.com/)
4. Commit changes to your `yml` file and push it to your bitbucket fork. Please keep your commits small and only edit one file per commit. This helps us quickly moderate changes. 
5. Create a "pull request" on bitbucket (great tutorials below).
6. The pull request will trigger a notification to the Z-Cloud site admins who will review it at their earliest convenience and merge it. 


### Notes:

1. Your pull request must be validated and approved before they appear on the main page. This just creates a little bit of QA buffer between contributions and the live site.
2. Bitbucket allows you to edit YML files online so technically you don't need to clone anything to change a tool.
3. If you are not comfortable using git,github,bitbucket etc. You can always make your changes manually and send us your `.yml` file in an email or, even better, pasting it into a [Secret Gist](https://gist.github.com/) 

### Additional Resources:

1. [Atlassian: Making a pull request](https://www.atlassian.com/git/tutorials/making-a-pull-request/)
1. [Bitbucket Pull Request VIDEO](https://www.youtube.com/watch?v=ssDHUyrQ8nI)
1. [Git & GitHub: Pull Requests](https://www.youtube.com/watch?v=FQsBmnZvBdc)
1. [Forking Vs. Spooning on bitbucket](https://www.youtube.com/watch?v=dYBjVTMUQY0)